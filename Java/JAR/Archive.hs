-- | This module defines functions to read Java JAR files.
module Java.JAR.Archive where

import qualified Codec.Archive.Zip as Zip
import Data.Binary
import Data.List
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString as SB
import qualified Data.Text as T
import System.FilePath

import Java.ClassPath.Types
import Java.ClassPath.Common
import JVM.ClassFile
import JVM.Converter

import qualified Data.Map as Map

-- readJAREntry :: (Enum a) => FilePath -> String -> IO (Maybe [a])
-- readJAREntry jarfile path = do
--   (Just `fmap` (Zip.withArchive jarfile $ ( <$> Zip.getEntry) =<< Zip.mkEntrySelector path))


zipFileNames :: Zip.ZipArchive [FilePath]
zipFileNames = fmap (fmap (T.unpack . Zip.getEntryName) . Map.keys) Zip.getEntries


-- | Read all entires from JAR file
readAllJAR :: FilePath -> IO [Tree CPEntry]
readAllJAR jarfile = do
    files <- Zip.withArchive jarfile $ zipFileNames
    return $ mapF (NotLoadedJAR jarfile) (buildTree $ filter good files)
  where
    good file = ".class" `isSuffixOf` file

jarContent :: FilePath -> Zip.ZipArchive B.ByteString
jarContent path = B.fromStrict <$> (Zip.getEntry =<< (Zip.mkEntrySelector path))

-- | Read one class from JAR file
readFromJAR :: FilePath -> FilePath -> IO (Class Direct)
readFromJAR jarfile path = do
  content <- Zip.withArchive jarfile $ jarContent path
  return $ classFile2Direct (decode content)

checkClassTree :: [Tree CPEntry] -> IO [Tree (FilePath, Class Direct)]
checkClassTree forest = mapFMF check forest
  where
    check _ (NotLoaded path) = do
       cls <- parseClassFile path
       return (path, cls)
    check a (Loaded path cls) = return (a </> path, cls)
    check a (NotLoadedJAR jar path) = do
       cls <- readFromJAR jar (a </> path)
       return (a </> path, cls)
    check a (LoadedJAR _ cls) =
       return (a </> show (thisClass cls), cls)

zipJAR :: [Tree (FilePath, Class Direct)] -> Zip.ZipArchive ()
zipJAR forest = do
    mapFM go forest
    return ()
  where
    go (path, cls) = Zip.addEntry Zip.Deflate (B.toStrict (encodeClass cls)) =<< (Zip.mkEntrySelector path)
--    go (path, cls) = Zip.addEnty path =<< Zip.sourceBuffer (B.unpack $ encodeClass cls)
