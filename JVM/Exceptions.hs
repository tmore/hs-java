{-# LANGUAGE DeriveDataTypeable, ExistentialQuantification #-}
module JVM.Exceptions where

import Control.Monad.Except
import qualified Data.ByteString.Lazy as B
import Data.Typeable (Typeable)

import JVM.ClassFile

data JVMError = NoItemInPool String
                   | UnexpectedEndMethod
                   | ClassFileNotLoaded FilePath
                   | JARNotLoaded FilePath String
                   | ClassNotFound String
                   | FieldNotFound String B.ByteString
                   | MethodNotFound String B.ByteString

instance Show JVMError where
  show (NoItemInPool s) = "No such item in pool" ++ s
  show UnexpectedEndMethod = "endMethod without startMethod!"
  show (ClassFileNotLoaded p) = "Class file was not loaded: " ++ p
  show (JARNotLoaded p c) = "Class was not loaded from JAR: " ++ p ++ ": " ++ c
  show (ClassNotFound p) = "No such class in ClassPath: " ++ p
  show (FieldNotFound c f) = "No such field in class " ++ c ++ ": " ++ toString f
  show (MethodNotFound c m) = "No such method in class " ++ c ++ ": " ++ toString m


-- instance Exception ENotFound

force :: (Show e) => String -> Except e a -> a
force s x =
   case runExcept x of
    Right result -> result
    Left  exc    -> error $ "Exception at " ++ s ++ ": " ++ show exc
