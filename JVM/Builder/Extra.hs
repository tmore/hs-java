{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-|
Author: Tomas Möre 2019

This library contains extra features and utilities can ease code generation

-}

module JVM.Builder.Extra where

import JVM.Builder.Monad
import JVM.Builder.Instructions

import qualified Java.Lang

import JVM.Exceptions
import JVM.ClassFile
import JVM.Assembler

import Data.Word
import Data.Int
import Data.Bits
import qualified Data.ByteString.Lazy as B

-- | Create an init method that allways initialises the super class.
newInitMethod :: Generator JVMError g
              => g ()
              -> g Word16
newInitMethod generator = do
   initName <- newMethod [ACC_PUBLIC] "<init>" [] ReturnsVoid $ do
     increaseStackSize 1
     aload_ I0
     className <- getSuperClassName
     invokeSpecial className Java.Lang.objectInit

     generator
     i0 RETURN
   className <- getClassName
   addToPool (CMethod className initName)

-- Psuedo instructions

i8push :: Generator e g => Int8 -> g ()
i8push = bipush . fromIntegral

i16push :: Generator e g => Int16 -> g ()
i16push = sipush . fromIntegral

i32push :: Generator e g => Int32 -> g ()
i32push n = do
  sipush upperPart
  sipush lowerPart
  where
    bitN  = fromIntegral n :: Word32
    lowerPart = fromIntegral bitN
    upperPart = fromIntegral (shiftR bitN 32)


-- | Create a new object, initates it and leaves a copy of the object references
-- on the stack
newInitiated :: Generator e g => B.ByteString -> g ()
newInitiated name = do
  new name
  dup
  invokeSpecial name Java.Lang.objectInit

class IPush n where
  ipush :: Generator e g => n -> g ()

instance IPush Int8 where
  ipush = i8push

instance IPush Int16 where
  ipush = i16push

instance IPush Int32 where
  ipush = i32push
