{-# LANGUAGE FlexibleContexts, TypeFamilies, OverloadedStrings, TypeSynonymInstances, FlexibleInstances, MultiParamTypeClasses, UndecidableInstances, GeneralizedNewtypeDeriving, ScopedTypeVariables, ConstraintKinds, LambdaCase #-}
-- | This module defines Generate[IO] monad, which helps generating JVM code and
-- creating Java class constants pool.
--
-- Code generation could be done using one of two monads: Generate and GenerateIO.
-- Generate monad is pure (simply State monad), while GenerateIO is IO-related.
-- In GenerateIO additional actions are available, such as setting up ClassPath
-- and loading classes (from .class files or JAR archives).
--
module JVM.Builder.Monad  where

import Prelude hiding (catch)
import Control.Monad.State as St
import Control.Monad.Except
import Data.Functor.Identity
import Data.Word
import Data.Binary
import Data.Binary.Put
import Data.Maybe

import Data.Map (Map)
import qualified Data.Map as Map
import qualified Data.Set as S
import qualified Data.ByteString.Lazy as B

import qualified Data.BinaryState as BS

import Data.Sequence (Seq(..), (<|), (|>))
import qualified Data.Sequence as Seq
import Data.Foldable (toList)


import JVM.Common
import JVM.ClassFile
import JVM.Assembler
import JVM.Exceptions
import Java.ClassPath


-- | The labels are currently just ints, when represented inside instruction
-- they may be cast to whatever integer type. This may be a source of potential
-- bugs because of precision.
newtype Label = Label Int
  deriving (Eq, Ord, Show)

-- | Gets the internal representation of the label such that it may temporarily
-- be used inside instructions
fromLabel :: Integral n => Label -> n
fromLabel (Label i) = fromIntegral i

-- | A labled instruction is either a label or a proper instruction. Labels are
-- used to calculate the proper offset when constructing the actuall class.
-- Labels do no count towards the size of the ouput and are entirely removed in
-- the final step.
data LInstruction = Lbl Label
                  | Instr Instruction
                  deriving (Eq, Show)

-- | Generator state
data GState =
  GState
  { _className :: B.ByteString        -- ^ Name of the class beeing
                                             -- generated, should not be modified
  , _superClass :: B.ByteString  -- ^ Name of any super class, defaults to object
  , _generatedCode   :: Seq LInstruction     -- ^ Already generated code (in current
                                            -- method)
  , _currentClassFields :: Seq (Field Direct) -- ^ All fields we've created in

  , _currentPool    :: Pool Direct           -- ^ Already generated constants pool
  , _nextPoolIndex  :: Word16                -- ^ Next index to be used in constants pool
  , _doneMethods    :: [Method Direct]       -- ^ Already generated class methods
  , _currentMethod  :: Maybe (Method Direct) -- ^ Current method
  , _stackSize      :: Word16                -- ^ Maximum stack size for current method
  , _locals         :: Word16                -- ^ Maximum number of local variables for
                                            -- current method
  , _classPath     :: [Tree CPEntry]
  , _nextLabel     :: Label                  -- ^ The next label to use, is reset                                            -- between method constructions
 } deriving (Eq,Show)

-- | Empty generator state
emptyGState ::  GState
emptyGState = GState
  { _className = ""
  , _superClass = "java/lang/Object"
  , _generatedCode = mempty
  , _currentClassFields = mempty
  , _currentPool = Map.empty
  , _nextPoolIndex = 1
  , _doneMethods = []
  , _currentMethod = Nothing
  , _stackSize = 496
  , _locals = 0
  , _classPath = []
  , _nextLabel = Label 0
  }

class Monad m => GeneratorMonad m where
  getGState :: m GState
  putGState :: GState -> m ()

instance (Monad m, MonadState GState m) => GeneratorMonad m where
  getGState = St.get
  putGState = St.put

modifyGState :: GeneratorMonad m => (GState -> GState) -> m ()
modifyGState fn = do
  st <- getGState
  putGState $ fn st

getsGState :: GeneratorMonad m => (GState -> a) -> m a
getsGState fn = do
  st <- getGState
  return $ fn st


getClassName :: GeneratorMonad m => m B.ByteString
getClassName = getsGState _className

getSuperClassName :: GeneratorMonad m => m B.ByteString
getSuperClassName = getsGState _superClass


type Generator e g = (GeneratorMonad g, MonadError e g)

-- | Generate monad
newtype Generate e a = Generate {
  runGenerate :: ExceptT e (State GState) a }
  deriving (Functor, Applicative, Monad, MonadState GState, MonadError e)



-- | IO version of Generate monad
newtype GenerateIO e a = GenerateIO {
  runGenerateIO :: ExceptT e (StateT GState IO) a }
  deriving (Functor, Applicative, Monad, MonadIO, MonadState GState, MonadError e)

execGenerateIO :: (Show e)
               => B.ByteString
               -> [Tree CPEntry]
               -> GenerateIO e a
               -> IO GState
execGenerateIO cls cp (GenerateIO emt) = do
    let caught = emt `catchError` (\ e -> fail $ show e)
    execStateT (runExceptT caught) (emptyGState {_classPath = cp
                                                ,_className = cls
                                                 })

execGenerate :: (Show e)
             => B.ByteString
             -> [Tree CPEntry]
             -> Generate e a
             -> GState
execGenerate cls cp (Generate emt) = do
    let caught = emt `catchError` (\e -> fail $ show e)
    execState (runExceptT caught) (emptyGState {_classPath = cp
                                               , _className = cls
                                               })

-- | Update ClassPath
withClassPath :: ClassPath () -> GenerateIO e ()
withClassPath cp = do
  res <- liftIO $ execClassPath cp
  st <- getGState
  putGState $ st {_classPath = res}

-- | Add a constant to pool
addItem :: (Generator e m) => Constant Direct -> m Word16
addItem c = do
  pool <- getsGState _currentPool
  case lookupPool c pool of
    Just i -> return i
    Nothing -> do
      i <- getsGState _nextPoolIndex
      let pool' = Map.insert i c pool
          i' = if long c
                 then i+2
                 else i+1
      modifyGState $ \st ->
            st {_currentPool = pool',
                _nextPoolIndex = i'}
      return i

-- | Lookup in a pool
lookupPool :: Constant Direct -> Pool Direct -> Maybe Word16
lookupPool c pool =
  fromIntegral `fmap` mapFindIndex (== c) pool

addNT :: (Generator e m, HasSignature a) => NameType a -> m Word16
addNT (NameType name sig) = do
  let bsig = encode sig
  x <- addItem (CNameType name bsig)
  addItem (CUTF8 name)
  addItem (CUTF8 bsig)
  return x

addSig :: (Generator e m) => MethodSignature -> m Word16
addSig c@(MethodSignature args ret) = do
  let bsig = encode c
  addItem (CUTF8 bsig)

-- | Add a constant into pool
addToPool :: (Generator e m) => Constant Direct -> m Word16
addToPool c@(CClass str) = do
  addItem (CUTF8 str)
  addItem c
addToPool c@(CField cls name) = do
  addToPool (CClass cls)
  addNT name
  addItem c
addToPool c@(CMethod cls name) = do
  addToPool (CClass cls)
  addNT name
  addItem c
addToPool c@(CIfaceMethod cls name) = do
  addToPool (CClass cls)
  addNT name
  addItem c
addToPool c@(CString str) = do
  addToPool (CUTF8 str)
  addItem c
addToPool c@(CNameType name sig) = do
  addItem (CUTF8 name)
  addItem (CUTF8 sig)
  addItem c
addToPool c = addItem c


newField :: Generator e g => Field Direct -> g Word16
newField f = do
  cName <- getClassName
  idx <- addToPool (CField cName (NameType (fieldName f) (fieldSignature f)))
  modifyGState (\ s -> s{ _currentClassFields =
                            _currentClassFields s |> f
                        })
  pure idx

-- getField :: Generator e g => B.ByteString -> g Word16
-- getField fieldName = do
--   fieldMap <- getsGState _currentClassFieldMap
--   case Map.lookup fieldName fieldMap of
--     Just a -> pure a
--     Nothing -> error "Field name was not defined"


-- ^ Allocated a new label for later use, does not insert the labewl inte do the
-- code flow directly. The `putLabel` instruction should be used for this
newLabel :: Generator e g => g Label
newLabel = do
  l@(Label i) <- getsGState _nextLabel
  modifyGState $ \st -> st {_nextLabel = Label $ i + 1}
  pure l

-- ^ Puts the label into the code sequence. Warning, this operation should only
-- be done once per label, if it happes several times the later position will be
-- used.
putLabel :: Generator e g => Label -> g ()
putLabel lbl =
  modifyGState (\st -> st { _generatedCode = _generatedCode st |> Lbl lbl})

putInstruction :: (Generator e g) => Instruction -> g ()
putInstruction instr = do
  modifyGState $ \st -> st {_generatedCode = _generatedCode st |> Instr instr}


-- | Generate one (zero-arguments) instruction
i0 :: (Generator e g) => Instruction -> g ()
i0 = putInstruction

-- | Generate one one-argument instruction
i1 :: (Generator e g) => (Word16 -> Instruction) -> Constant Direct -> g ()
i1 fn c = do
  ix <- addToPool c
  i0 (fn ix)

-- | Generate one one-argument instruction
i8 :: (Generator e g) => (Word8 -> Instruction) -> Constant Direct -> g ()
i8 fn c = do
  ix <- addToPool c
  i0 (fn $ fromIntegral ix)


-- | Set maximum stack size for current method
setStackSize :: (Generator e g) => Word16 -> g ()
setStackSize n = do
  modifyGState $ \st -> st {_stackSize = n}

-- | Increases the stack size
increaseStackSize :: Generator e g => Word16 -> g ()
increaseStackSize n =
  modifyGState $ \st -> st {_stackSize = _stackSize st + n}

-- | Set maximum number of local variables for current method
setMaxLocals :: (Generator e g) => Word16 -> g ()
setMaxLocals n = do
  modifyGState $ \st -> st {_locals = n}

-- | Increases the max locals parameter
increaseSetMaxLocals :: Generator e g => Word16 -> g ()
increaseSetMaxLocals n = do
  modifyGState $ \st -> st {_locals = _locals st + n}

-- | Start generating new method
startMethod :: (Generator e g) => [AccessFlag] -> B.ByteString -> MethodSignature -> g ()
startMethod flags name sig = do
  addToPool (CString name)
  addSig sig
  setStackSize 4096
  setMaxLocals 100
  st <- getGState
  let method = Method {
    methodAccessFlags = S.fromList flags,
    methodName = name,
    methodSignature = sig,
    methodAttributesCount = 0,
    methodAttributes = AR Map.empty }
  putGState $ st { _generatedCode = mempty
                 , _currentMethod = Just method
                 , _nextLabel = Label 0
                 }

-- | End of method generation
endMethod :: (Generator JVMError g) => g ()
endMethod = do
  m <- getsGState _currentMethod
  code <- getsGState genCode
  case m of
    Nothing -> throwError UnexpectedEndMethod
    Just method -> do
      let method' = method {methodAttributes = AR $
                             Map.fromList [("Code"
                                           , encodeMethod code
                                           )
                                          ],
                            methodAttributesCount = 1}
      modifyGState $ \st ->
               st {  _generatedCode = mempty
                   , _currentMethod = Nothing
                   , _doneMethods = _doneMethods st ++ [method']}

byteLength :: Integral n => Instruction -> n
byteLength instr = fromIntegral $ fst $ runPutM $ evalStateT  ((BS.put instr) >> BS.getOffset) 0

-- | Translates LInstruction to real instructions
lInstructionToInstruction :: Seq LInstruction -> [Instruction]
lInstructionToInstruction lInstrs =
  toList $ translate  lInstrs
  where

    createLabelMap :: Seq LInstruction -> Map Label Int
    createLabelMap = go 0
      where
        go :: Int -> Seq LInstruction -> Map Label Int
        go pos =
          \case
            Seq.Empty -> Map.empty
            (Lbl l) :<| xs -> Map.insert l pos (go pos xs)
            (Instr x) :<| xs ->
              let offset = byteLength x
              in (go (pos + offset) xs)
    labelMap = createLabelMap lInstrs
    translate :: Seq LInstruction
              -> Seq Instruction
    translate =
      let go posInMethod =
            \case
              Seq.Empty -> Seq.empty
              (Lbl _) :<| rest -> go posInMethod rest
              (Instr x) :<| xs ->
                let offset = byteLength x
                in translateInst posInMethod x <| go (posInMethod + offset) xs
      in go 0
    translateInst posInMethod lInst =
      case lInst of
        IF cmp label ->
          IF
            cmp
            (calcOffset
               (labelMap Map.! (Label (fromIntegral label)))
               posInMethod)
        IF_ICMP cmp label ->
          IF_ICMP
            cmp
            (calcOffset
               (labelMap Map.! (Label (fromIntegral label)))
               posInMethod)
        IF_ACMP cmp label ->
          IF_ACMP
            cmp
            (calcOffset
               (labelMap Map.! (Label (fromIntegral label)))
               posInMethod)
        GOTO label ->
          GOTO
            (calcOffset
               (labelMap Map.! (Label (fromIntegral label)))
               posInMethod)
        JSR label ->
          JSR
            (calcOffset
               (labelMap Map.! (Label (fromIntegral label)))
               posInMethod)
        i -> i
    calcOffset :: Int -> Int -> Word16
    calcOffset labelPosInMethod posInMethod =
      fromIntegral (labelPosInMethod - posInMethod)

-- | Generate new method
newMethod :: (Generator JVMError g)
          => [AccessFlag]        -- ^ Access flags for method (public, static etc)
          -> B.ByteString        -- ^ Method name
          -> [ArgumentSignature] -- ^ Signatures of method arguments
          -> ReturnSignature     -- ^ Method return signature
          -> g ()                -- ^ Generator for method code
          -> g (NameType (Method Direct))
newMethod flags name args ret gen = do
  let sig = MethodSignature args ret
  startMethod flags name sig
  gen
  endMethod
  return (NameType name sig)




-- | Get a class from current ClassPath
getClass :: String
         -> GenerateIO JVMError (Class Direct)
getClass name = do
  cp <- getsGState _classPath
  res <- liftIO $ getEntry cp name
  case res of
    Just (NotLoaded p) -> throwError $ ClassFileNotLoaded p
    Just (Loaded _ c) -> return c
    Just (NotLoadedJAR p c) -> throwError $ JARNotLoaded p c
    Just (LoadedJAR _ c) -> return c
    Nothing -> throwError $ ClassNotFound name

-- | Get class field signature from current ClassPath
getClassField :: String -> B.ByteString -> GenerateIO JVMError (NameType (Field Direct))
getClassField clsName fldName = do
  cls <- getClass clsName
  case lookupField fldName cls of
    Just fld -> return (fieldNameType fld)
    Nothing  -> throwError $ FieldNotFound clsName fldName

-- | Get class method signature from current ClassPath
getClassMethod :: String -> B.ByteString -> GenerateIO JVMError (NameType (Method Direct))
getClassMethod clsName mName = do
  cls <- getClass clsName
  case lookupMethod mName cls of
    Just m -> return (methodNameType m)
    Nothing  -> throwError $ MethodNotFound clsName mName

-- | Access the generated bytecode length
encodedCodeLength :: GState -> Word32
encodedCodeLength st =
  fromIntegral . B.length . encodeInstructions $ lInstructionToInstruction $ _generatedCode st

--generateCodeLength :: (Show e) => Generate e a -> Word32
--generateCodeLength = encodedCodeLength . execGenerate []

-- | Convert Generator state to method Code.
genCode :: GState -> Code
genCode st = Code {
    codeStackSize = _stackSize st,
    codeMaxLocals = _locals st,
    codeLength = encodedCodeLength st,
    codeInstructions = lInstructionToInstruction $ _generatedCode st,
    codeExceptionsN = 0,
    codeExceptions = [],
    codeAttrsN = 0,
    codeAttributes = AP [] }

-- | Start class generation.
initClass :: (Show e, Generator e g)
          => B.ByteString
          -> B.ByteString
          -> g Word16
initClass name superClass = do
  modifyGState $ \st -> st { _superClass = superClass}
  addToPool (CClass superClass)
  addToPool (CClass name)
  addToPool (CString "Code")

-- | Generate a class
generateM
  :: (Generator e m, Show e)
  => [Tree CPEntry] -- ^ Class path
  -> B.ByteString -- ^ Class name
  -> Maybe B.ByteString -- ^ Superclass
  -> m () -- ^ The builder
  -> m (Class Direct)
generateM cp name mSuperClass gen = do
  origGState <- getGState
  res <-
    do modifyGState $ \_ ->
         emptyGState
         { _classPath = cp
         , _className = name
         , _superClass = fromMaybe "java/lang/Object" mSuperClass
         }
       initClass name (fromMaybe "java/lang/Object" mSuperClass)
       gen
       getGState
  putGState origGState
  let d = defaultClass :: Class Direct
  return $
    d
    { classFieldsCount = fromIntegral (Seq.length (_currentClassFields res))
    , classFields = toList (_currentClassFields res)
    , constsPoolSize = fromIntegral $ Map.size (_currentPool res)
    , constsPool = _currentPool res
    , accessFlags = S.fromList [ACC_PUBLIC, ACC_STATIC]
    , thisClass = name
    , superClass = _superClass res
    , classMethodsCount = fromIntegral $ length (_doneMethods res)
    , classMethods = _doneMethods res
    }

-- | Generate a class
generateIO :: (Show e)
           => [Tree CPEntry] -- ^ Class path
           -> B.ByteString   -- ^ Class name
           -> Maybe B.ByteString   -- ^ Superclass
           -> GenerateIO e () -- ^ The builder
           -> IO (Class Direct)
generateIO cp name mSuperClass gen = do
  eRes <-
    flip evalStateT emptyGState $
    runExceptT $ runGenerateIO $
    generateM cp name mSuperClass gen
  case eRes of
    Left err -> error $ show err
    Right r -> pure r

-- | Generate a class
generate :: (Show e)
           => [Tree CPEntry] -- ^ Class path
           -> B.ByteString   -- ^ Class name
           -> Maybe B.ByteString   -- ^ Superclass
           -> Generate e () -- ^ The builder
           -> Either e (Class Direct)
generate cp name mSuperClass gen =
  let (Identity eRes) =
        flip evalStateT emptyGState $
        runExceptT $ runGenerate $
        generateM cp name mSuperClass gen
  in eRes
