-- | This module exports shortcuts for some of JVM instructions (which are defined in JVM.Assembler).
-- These functions get Constants, put them into constants pool and generate instruction using index
-- of constant in the pool.
module JVM.Builder.Instructions where

import Data.Word
import qualified Data.ByteString.Lazy as B
import Codec.Binary.UTF8.String (encodeString)
import Data.String

import JVM.ClassFile
import JVM.Assembler
import JVM.Builder.Monad

-- Added during insertion of label counting

if_icmp :: Generator e g => CMP -> Label -> g ()
if_icmp cmp (Label i) = i0 (IF_ICMP cmp (fromIntegral i))

{-| Is value on stack is 0, jump to label -}
if_eq :: Generator e g => Label -> g ()
if_eq (Label i) = i0 (IF C_EQ (fromIntegral i))

{-| Is value on stack is not 0, jump to label -}
if_neq :: Generator e g => Label -> g ()
if_neq (Label i) = i0 (IF C_NE (fromIntegral i))

goto :: Generator e g => Label -> g ()
goto (Label i) = i0 (GOTO (fromIntegral i))

--

nop :: Generator e g => g ()
nop = i0 NOP
aconst_null :: Generator e g => g ()
aconst_null = i0 ACONST_NULL
iconst_m1 :: Generator e g => g ()
iconst_m1 = i0 ICONST_M1
iconst_0 :: Generator e g => g ()
iconst_0 = i0 ICONST_0
iconst_1 :: Generator e g => g ()
iconst_1 = i0 ICONST_1
iconst_2 :: Generator e g => g ()
iconst_2 = i0 ICONST_2
iconst_3 :: Generator e g => g ()
iconst_3 = i0 ICONST_3
iconst_4 :: Generator e g => g ()
iconst_4 = i0 ICONST_4
iconst_5 :: Generator e g => g ()
iconst_5 = i0 ICONST_5
lconst_0 :: Generator e g => g ()
lconst_0 = i0 LCONST_0
lconst_1 :: Generator e g => g ()
lconst_1 = i0 LCONST_1
fconst_0 :: Generator e g => g ()
fconst_0 = i0 FCONST_0
fconst_1 :: Generator e g => g ()
fconst_1 = i0 FCONST_1
fconst_2 :: Generator e g => g ()
fconst_2 = i0 FCONST_2
dconst_0 :: Generator e g => g ()
dconst_0 = i0 DCONST_0
dconst_1 :: Generator e g => g ()
dconst_1 = i0 DCONST_1

bipush :: Generator e g => Word8 -> g ()
bipush x = i0 (BIPUSH x)
sipush :: Generator e g => Word16 -> g ()
sipush x = i0 (SIPUSH x)



ldc1 :: Generator e g => Constant Direct -> g ()
ldc1 x = i8 LDC1 x
ldc2 :: Generator e g => Constant Direct -> g ()
ldc2 x = i1 LDC2 x
ldc2w :: Generator e g => Constant Direct -> g ()
ldc2w x = i1 LDC2W x

iload :: Generator e g => Word8 -> g ()
iload x = i0 (ILOAD x)
lload :: Generator e g => Word8 -> g ()
lload x = i0 (LLOAD x)
fload :: Generator e g => Word8 -> g ()
fload x = i0 (FLOAD x)
dload :: Generator e g => Word8 -> g ()
dload x = i0 (DLOAD x)
aload :: Generator e g => Word8 -> g ()
aload x = i0 (ALOAD x)

iload_ :: Generator e g => IMM -> g ()
iload_ x = i0 (ILOAD_ x)
lload_ :: Generator e g => IMM -> g ()
lload_ x = i0 (LLOAD_ x)
fload_ :: Generator e g => IMM -> g ()
fload_ x = i0 (FLOAD_ x)
dload_ :: Generator e g => IMM -> g ()
dload_ x = i0 (DLOAD_ x)
aload_ :: Generator e g => IMM -> g ()
aload_ x = i0 (ALOAD_ x)

iaload :: Generator e g => g ()
iaload = i0 IALOAD
laload :: Generator e g => g ()
laload = i0 LALOAD
faload :: Generator e g => g ()
faload = i0 FALOAD
daload :: Generator e g => g ()
daload = i0 DALOAD
aaload :: Generator e g => g ()
aaload = i0 AALOAD
caload :: Generator e g => g ()
caload = i0 CALOAD
saload :: Generator e g => g ()
saload = i0 SALOAD

istore :: Generator e g => Constant Direct -> g ()
istore x = i8 ISTORE x
lstore :: Generator e g => Constant Direct -> g ()
lstore x = i8 LSTORE x
fstore :: Generator e g => Constant Direct -> g ()
fstore x = i8 FSTORE x
dstore :: Generator e g => Constant Direct -> g ()
dstore x = i8 DSTORE x
astore :: Generator e g => Constant Direct -> g ()
astore x = i8 ASTORE x

istore_ :: Generator e g => Word8 -> g ()
istore_ x = i0 (ISTORE x)
lstore_ :: Generator e g => Word8 -> g ()
lstore_ x = i0 (LSTORE x)
fstore_ :: Generator e g => Word8 -> g ()
fstore_ x = i0 (FSTORE x)
dstore_ :: Generator e g => Word8 -> g ()
dstore_ x = i0 (DSTORE x)
astore_ :: Generator e g => Word8 -> g ()
astore_ x = i0 (ASTORE x)

iastore :: Generator e g => g ()
iastore = i0 IASTORE
lastore :: Generator e g => g ()
lastore = i0 LASTORE
fastore :: Generator e g => g ()
fastore = i0 FASTORE
dastore :: Generator e g => g ()
dastore = i0 DASTORE
aastore :: Generator e g => g ()
aastore = i0 AASTORE
bastore :: Generator e g => g ()
bastore = i0 BASTORE
castore :: Generator e g => g ()
castore = i0 CASTORE
sastore :: Generator e g => g ()
sastore = i0 SASTORE

pop :: Generator e g => g ()
pop     = i0 POP
pop2 :: Generator e g => g ()
pop2    = i0 POP2
dup :: Generator e g => g ()
dup     = i0 DUP
dup_x1 :: Generator e g => g ()
dup_x1  = i0 DUP_X1
dup_x2 :: Generator e g => g ()
dup_x2  = i0 DUP_X2
dup2 :: Generator e g => g ()
dup2    = i0 DUP2
dup2_x1 :: Generator e g => g ()
dup2_x1 = i0 DUP2_X1
dup2_x2 :: Generator e g => g ()
dup2_x2 = i0 DUP2_X2
swap :: Generator e g => g ()
swap    = i0 SWAP
iadd :: Generator e g => g ()
iadd    = i0 IADD
ladd :: Generator e g => g ()
ladd    = i0 LADD
fadd :: Generator e g => g ()
fadd    = i0 FADD
dadd :: Generator e g => g ()
dadd    = i0 DADD
isub :: Generator e g => g ()
isub    = i0 ISUB
lsub :: Generator e g => g ()
lsub    = i0 LSUB
fsub :: Generator e g => g ()
fsub    = i0 FSUB
dsub :: Generator e g => g ()
dsub    = i0 DSUB
imul :: Generator e g => g ()
imul    = i0 IMUL
lmul :: Generator e g => g ()
lmul    = i0 LMUL
fmul :: Generator e g => g ()
fmul    = i0 FMUL
dmul :: Generator e g => g ()
dmul    = i0 DMUL
idiv :: Generator e g => g ()
idiv    = i0 IDIV
ldiv :: Generator e g => g ()
ldiv    = i0 LDIV
fdiv :: Generator e g => g ()
fdiv    = i0 FDIV
ddiv :: Generator e g => g ()
ddiv    = i0 DDIV
irem :: Generator e g => g ()
irem    = i0 IREM
lrem :: Generator e g => g ()
lrem    = i0 LREM
frem :: Generator e g => g ()
frem    = i0 FREM
drem :: Generator e g => g ()
drem    = i0 DREM
ineg :: Generator e g => g ()
ineg    = i0 INEG
lneg :: Generator e g => g ()
lneg    = i0 LNEG
fneg :: Generator e g => g ()
fneg    = i0 FNEG
dneg :: Generator e g => g ()
dneg    = i0 DNEG
ishl :: Generator e g => g ()
ishl    = i0 ISHL
lshl :: Generator e g => g ()
lshl    = i0 LSHL
ishr :: Generator e g => g ()
ishr    = i0 ISHR
lshr :: Generator e g => g ()
lshr    = i0 LSHR
iushr :: Generator e g => g ()
iushr   = i0 IUSHR
lushr :: Generator e g => g ()
lushr   = i0 LUSHR
iand :: Generator e g => g ()
iand    = i0 IAND
land :: Generator e g => g ()
land    = i0 LAND
ior :: Generator e g => g ()
ior     = i0 IOR
lor :: Generator e g => g ()
lor     = i0 LOR
ixor :: Generator e g => g ()
ixor    = i0 IXOR
lxor :: Generator e g => g ()
lxor    = i0 LXOR

iinc :: Generator e g => Word8 -> Word8 -> g ()
iinc x y = i0 (IINC x y)

i2l :: Generator e g => g ()
i2l  = i0 I2L
i2f :: Generator e g => g ()
i2f  = i0 I2F
i2d :: Generator e g => g ()
i2d  = i0 I2D
l2i :: Generator e g => g ()
l2i  = i0 L2I
l2f :: Generator e g => g ()
l2f  = i0 L2F
l2d :: Generator e g => g ()
l2d  = i0 L2D
f2i :: Generator e g => g ()
f2i  = i0 F2I
f2l :: Generator e g => g ()
f2l  = i0 F2L
f2d :: Generator e g => g ()
f2d  = i0 F2D
d2i :: Generator e g => g ()
d2i  = i0 D2I
d2l :: Generator e g => g ()
d2l  = i0 D2L
d2f :: Generator e g => g ()
d2f  = i0 D2F
i2b :: Generator e g => g ()
i2b  = i0 I2B
i2c :: Generator e g => g ()
i2c  = i0 I2C
i2s :: Generator e g => g ()
i2s  = i0 I2S
lcmp :: Generator e g => g ()
lcmp = i0 LCMP

-- | Wide instruction
wide :: Generator e g => (Word8 -> Instruction) -> Constant Direct -> g ()
wide fn c = do
  ix <- addToPool c
  let ix0 = fromIntegral (ix `div` 0x100) :: Word8
      ix1 = fromIntegral (ix `mod` 0x100) :: Word8
  i0 (WIDE ix0 $ fn ix1)

new :: Generator e g => B.ByteString -> g ()
new cls =
  i1 NEW (CClass cls)

newArray :: Generator e g => ArrayType -> g ()
newArray t =
  i0 (NEWARRAY $ atype2byte t)

allocNewArray :: Generator e g => B.ByteString -> g ()
allocNewArray cls =
  i1 ANEWARRAY (CClass cls)

invokeVirtual :: Generator e g => B.ByteString -> NameType (Method Direct) -> g ()
invokeVirtual cls sig =
  i1 INVOKEVIRTUAL (CMethod cls sig)

invokeVirtual_ :: Generator e g => Word16 -> g ()
invokeVirtual_ x =
  i0 (INVOKEVIRTUAL x)

invokeStatic :: Generator e g => B.ByteString -> NameType (Method Direct) -> g ()
invokeStatic cls sig =
  i1 INVOKESTATIC (CMethod cls sig)

invokeSpecial :: Generator e g => B.ByteString -> NameType (Method Direct) -> g ()
invokeSpecial cls sig =
  i1 INVOKESPECIAL (CMethod cls sig)

getStaticField :: Generator e g => B.ByteString -> NameType (Field Direct) -> g ()
getStaticField cls sig =
  i1 GETSTATIC (CField cls sig)

loadString :: Generator e g => String -> g ()
loadString str =
  i8 LDC1 (CString $ fromString $ encodeString $ str)

allocArray :: Generator e g => B.ByteString -> g ()
allocArray cls =
  i1 ANEWARRAY (CClass cls)

putfield :: Generator e g
         => B.ByteString -- ^ ClassName
         -> B.ByteString -- ^ FieldName
         -> FieldType    -- ^ Type of the variable
         -> g ()
putfield className fieldName fieldType = do
  fieldIdx <- addItem (CField className (NameType fieldName fieldType))
  i0 (PUTFIELD fieldIdx)

putfield_ :: Generator e g
          => Word16
          -> g ()
putfield_ idx = i0 (PUTFIELD idx)


getfield :: Generator e g
         => B.ByteString -- ^ ClassName
         -> B.ByteString -- ^ FieldName
         -> FieldType -- ^ Type of the variable
         -> g ()
getfield className fieldName fieldType = do
  fieldIdx <- addToPool (CField className (NameType fieldName fieldType))
  i0 (GETFIELD fieldIdx)

getfield_ :: Generator e g => Word16 -> g ()
getfield_ x =
  i0 (GETFIELD x)
